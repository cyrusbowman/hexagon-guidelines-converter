var gpsSim = require('node-gps-sim');
var moment = require('moment');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');
const commandLineUsage = require('command-line-usage')
const commandLineArgs = require('command-line-args')

const optionDefinitions = [
  {
    name: 'input',
    alias: 'i',
    type: String,
    typeLabel: '{underline file}',
    description: 'The Hexagon guideline file to process.'
  },
  {
    name: 'output',
    alias: 'o',
    type: String,
    typeLabel: '{underline filename}',
    description: '(optional) The {underline filename} for the converted file.'
  },
  {
    name: 'time',
    alias: 't',
    type: String,
    typeLabel: '{underline hh:mm:ss}',
    description: '(optional) The time for the first gpgga hh:mm:ss.'
  },
  {
    name: 'help',
    description: 'Print this usage guide.'
  }
]
const options = commandLineArgs(optionDefinitions)

const sections = [
  {
    header: 'Hexagon Guidelines Converter',
    content: 'Converts Hexagons guideline files to GPGGA format.'
  },
  {
    header: 'Options',
    optionList: optionDefinitions
  }
]

if (options.input === undefined || options.help !== undefined) {
  const usage = commandLineUsage(sections)
  console.log(usage)
  return;
}

if (options.output === undefined) {
  var base = path.basename(options.input);
  options.output = path.dirname(options.input)+'/'+base.substr(0, base.length - path.extname(base).length) + "_gpgga" + path.extname(base);
}

var file = fs.readFileSync(options.input, "utf8");

var time = null;
if (options.time !== undefined) {
  var toParse = moment().utc().format('YYYY-MM-DD') + ' ' + options.time;
  time = moment.utc(toParse);
}
if (time == null || time.isValid() == false) {
  time = moment().utc();
}

file = file.replace(/\]\\n\[/g, ", ") //Remove weird "]\n[" in file
file = file.replace(/\"\[/g, "") //Remove array start at beginning.
file = file.replace(/\]\\n\"/g, "") //Remove newline at end.

file = file.split(',');

var allPoints = [];
var outputData = '';
var startTime = time.clone();
for (var i=0; i<file.length; i=i+2) {
  var longitude = file[i].trim();
  var latitude = file[i+1].trim();
  if(_.findIndex(allPoints, {latitude, longitude}) == -1) {
    allPoints.push({latitude, longitude});
    outputData+=gpsSim.gpsToGGA({latitude, longitude}, time)+'\n';
    time.add(1, 'second');
  }
}
if (outputData !== '' ) {
  fs.writeFile(options.output, outputData, function (err) {
    if (err) throw err;
    console.log('Done converting!');
    console.log('Start Time:', startTime.format('HH:mm:ss'))
    console.log('End Time:', time.format('HH:mm:ss'))
  });
}
